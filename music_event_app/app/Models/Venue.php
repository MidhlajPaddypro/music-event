<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    use HasFactory;
    protected $table = 'venue';
    protected $fillable = [
        'id',
        'name',
        'address',
        'contact_number',
        'status'
    ];

    public function event()
    {
        return $this->belongsToMany(Event::class,'venue_id');
    }
}
