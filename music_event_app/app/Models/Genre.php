<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
    protected $table = 'genre_master';


    protected $fillable = [
        'id',
        'name',
        'status'
    ];

    public function event()
    {
        return $this->belongsToMany(Event::class,'genre_id');
    }
}
