<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'title',
        'genre_id',
        'image',
        'short_description',
        'amount',
        'date',
        'venue_id',
        'artist_id',
        'status'
    ];

    public function genre()
    {
        return $this->hasOne(Genre::class,'id','genre_id');
    }

    public function artist()
    {
        return $this->hasOne(Artist::class,'id','artist_id');
    }

    public function venue()
    {
        return $this->hasOne(Venue::class,'id','venue_id');
    }



}
