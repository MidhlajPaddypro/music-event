<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    use HasFactory;
    protected $table = 'artist_master';
    // protected $primaryKey = 'artist_id';
    protected $fillable = [
        'id',
        'name',
        'status'
    ];

    public function event()
    {
        return $this->belongsToMany(Event::class,'artist_id');
    }
}
