<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Genre::get();
        return view('genre')->with('data', $data);
    }

    public function destroy(Request $request)
    {
        try {
            Genre::where('id', $request->id)->delete();
            return redirect()->route('genre')->with('successAlert', 'Successfully Deleted Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {

            $data = $request->validate([
                'genreName' => 'required'
            ]);

            $data = new Genre();
            $data->name = $request->genreName;
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('successAlert', 'Successfully Saved Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }


    public function update(Request $request, $id)
    {
        try {
            $data = $request->validate([
                'genreName' => 'required'
            ]);

            $data = Genre::where('id', $id)->first();
            $data->name = $request->genreName;
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('successAlert', 'Successfully Updated Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }
}
