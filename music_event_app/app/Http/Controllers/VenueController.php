<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use Illuminate\Http\Request;

class VenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Venue::orderBy('id', 'DESC')->Paginate(10);
        return view('venue')->with('data', $data);
    }

    public function destroy(Request $request)
    {
        try {
            Venue::where('id', $request->id)->delete();
            return redirect()->route('venue')->with('successAlert', 'Successfully Deleted Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'venueName' => 'required',
                'venueContact' => 'required|numeric|digits:10',
                'venueAddress' => 'required|min:10'
            ]);

            $data = new Venue();
            $data->name = $request->venueName;
            $data->contact_number = $request->venueContact;
            $data->address = $request->venueAddress;
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('successAlert', 'Successfully Saved Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $request->validate([
                'venueName' => 'required',
                'venueContact' => 'required|numeric|digits:10',
                'venueAddress' => 'required|min:10'
            ]);
            $data = Venue::where('id', $id)->first();
            $data->name = $request->venueName;
            $data->contact_number = $request->venueContact;
            $data->address = $request->venueAddress;
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('successAlert', 'Successfully Updated Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }
}
