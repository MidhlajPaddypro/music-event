<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Genre;
use App\Models\Artist;
use App\Models\Venue;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Event::with('venue')->with('artist')->with('genre')->orderBy('id', 'DESC')->paginate(20);
        return view('event')->with('data', $data);
    }

    public function destroy(Request $request)
    {
        try {
            Event::where('id', $request->id)->delete();
            return redirect()->route('event')->with('successAlert', 'Successfully Deleted Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function edit($id)
    {
        $result = Event::where('id', $id)->with('venue')->with('artist')->with('genre')->get();
        $genre = Genre::get();
        $artist = Artist::get();
        $venue = Venue::get();
        return view('eventEdit')->with('data', $result[0])->with('genre', $genre)->with('artist', $artist)->with('venue', $venue);
    }

    public function add()
    {
        $genre = Genre::get();
        $artist = Artist::get();
        $venue = Venue::get();
        return view('eventAdd')->with('genre', $genre)->with('artist', $artist)->with('venue', $venue);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'eventTitle' => 'required',
                'genre' => 'required',
                'artist' => 'required',
                'venue' => 'required',
                'date' => 'required|date',
                'amount' => 'required|numeric',
                'description' => 'required'
            ]);

            $data = new Event();
            $data->title = $request->eventTitle;
            $data->genre_id = $request->genre;
            $data->artist_id = $request->artist;
            $data->venue_id = $request->venue;
            $data->date = $request->date;
            $data->amount = $request->amount;
            $data->short_description = $request->description;
            $data->status = 1;

            if ($request->img != "") {
                $img = explode('|', $request->img);
                $ext = '';
                for ($i = 0; $i < count($img) - 1; $i++) {
                    $image_path = 'uploads/' . $data->image;
                    if (File::exists($image_path)) {
                        File::delete($image_path);
                    }
                    if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
                        $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);
                        $ext = '.jpg';
                    }
                    if (strpos($img[$i], 'data:image/png;base64,') === 0) {
                        $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]);
                        $ext = '.png';
                    }
                    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $pin = mt_rand(100, 9999) . mt_rand(100, 999) . $characters[rand(0, strlen($characters) - 1)];
                    $string = str_shuffle($pin);
                    $data->image = $string . $ext;
                    $data->save();
                    $img[$i] = str_replace(' ', '+', $img[$i]);
                    $data = base64_decode($img[$i]);
                    $temp_string2 = 'uploads/';
                    $file = $temp_string2 . $string . $ext;
                    if (file_put_contents($file, $data)) {
                        echo "<p>Image $i was saved as $file.</p>";
                    } else {
                        echo '<p>Image $i could not be saved.</p>';
                    }
                }
                return redirect()->route('event')->with('successAlert', 'Successfully Saved Record!');
            } else {
                $data->save();
                return redirect()->route('event')->with('successAlert', 'Successfully Saved Record!');
            }
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $request->validate([
                'eventTitle' => 'required',
                'genre' => 'required',
                'artist' => 'required',
                'venue' => 'required',
                'date' => 'required|date',
                'amount' => 'required|numeric',
                'description' => 'required'
            ]);

            $data = Event::where('id', $request->id)->first();

            $data->title = $request->eventTitle;
            $data->genre_id = $request->genre;
            $data->artist_id = $request->artist;
            $data->venue_id = $request->venue;
            $data->date = $request->date;
            $data->amount = $request->amount;
            $data->short_description = $request->description;
            $data->status = 1;

            if ($request->img != "") {
                $img = explode('|', $request->img);
                $ext = '';
                for ($i = 0; $i < count($img) - 1; $i++) {
                    $image_path = 'uploads/' . $data->image;
                    if (File::exists($image_path)) {
                        File::delete($image_path);
                    }
                    if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
                        $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);
                        $ext = '.jpg';
                    }
                    if (strpos($img[$i], 'data:image/png;base64,') === 0) {
                        $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]);
                        $ext = '.png';
                    }
                    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $pin = mt_rand(100, 9999) . mt_rand(100, 999) . $characters[rand(0, strlen($characters) - 1)];
                    $string = str_shuffle($pin);
                    $data->image = $string . $ext;
                    $data->save();
                    $img[$i] = str_replace(' ', '+', $img[$i]);
                    $data = base64_decode($img[$i]);
                    $temp_string2 = 'uploads/';
                    $file = $temp_string2 . $string . $ext;
                    if (file_put_contents($file, $data)) {
                        echo "<p>Image $i was saved as $file.</p>";
                    } else {
                        echo '<p>Image $i could not be saved.</p>';
                    }
                }
                return redirect()->route('event')->with('successAlert', 'Successfully Saved Record!');
            } else {
                $data->save();
                return redirect()->route('event')->with('successAlert', 'Successfully Saved Record!');
            }
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }
}
