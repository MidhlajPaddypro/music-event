<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Artist::get();
        return view('artist')->with('data', $data);
    }

    public function destroy(Request $request)
    {
        try {
            Artist::where('id', $request->id)->delete();
            return redirect()->route('artist')->with('successAlert', 'Successfully Deleted Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {

            $data = $request->validate([
                'artistName' => 'required'
            ]);

            $data = new Artist();

            $data->name = $request->artistName;
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('successAlert', 'Successfully Saved Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {

            $data = $request->validate([
                'artistName' => 'required'
            ]);

            $data = Artist::where('id', $id)->first();

            $data->name = $request->artistName;
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('successAlert', 'Successfully Updated Record!');
        } catch (\Throwable $error) {
            return redirect()->back()->withErrors($error->getMessage());
        }
    }
}
