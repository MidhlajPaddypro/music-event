<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Event;
use App\Models\Genre;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {
        $data = Event::with('venue', 'artist', 'genre')->orderBy('id', 'DESC')->cursorPaginate(20);
        $artist = Artist::get();
        $venue = Venue::get();
        $genre = Genre::get();
        return view('home')->with('data', $data)->with('artist', $artist)->with('venue', $venue)->with('genre', $genre);
    }




    public function search(Request $request)
    {
        $data = Event::where('title', 'LIKE', '%' . $request->searchmusic . '%')->with('venue')->with('artist')->with('genre')->orderBy('id', 'DESC')->get();
        $artist = Artist::get();
        $venue = Venue::get();
        $genre = Genre::get();
        return view('home')->with('data', $data)->with('search', $request->searchmusic)->with('artist', $artist)->with('venue', $venue)->with('genre', $genre);
    }

    public function filter(Request $request)
    {
           

        $artist = Artist::get();
        $venue = Venue::get();
        $genre = Genre::get();

        $req = array_keys($request->all());

        $artist_checked = $genre_checked = $venue_checked = array();
        $date_split = $newFromDate = $newToDate = "";

        foreach ($req as $val) {
            if (strncmp($val, "genre_checkbox_", 15) == 0)
                array_push($genre_checked, $request->$val);
            else if (strncmp($val, "artist_checkbox_", 16) == 0)
                array_push($artist_checked, $request->$val);
            else if (strncmp($val, "venue_checkbox_", 15) == 0)
                array_push($venue_checked, $request->$val);
            else if (strncmp($val, "date_filter", 11) == 0)
                {
                    $date_split = explode(" - ",$request->date_filter);
                    $newFromDate = date("Y-m-d", strtotime($date_split[0]));
                    $newToDate = date("Y-m-d", strtotime($date_split[1]));
                }
        }

        if (!empty($venue_checked)){
            $data = Event::whereIn('venue_id', $venue_checked)->with('venue')->with('artist')->with('genre')->orderBy('id', 'DESC')->get();
            return view('home')->with('data', $data)->with('venue_checked', $venue_checked)->with('artist', $artist)->with('venue', $venue)->with('genre', $genre);

        }
        else if (!empty($artist_checked)) {
            $data = Event::whereIn('artist_id', $artist_checked)->with('venue')->with('artist')->with('genre')->orderBy('id', 'DESC')->get();
            return view('home')->with('data', $data)->with('artist_checked', $artist_checked)->with('artist', $artist)->with('venue', $venue)->with('genre', $genre);

        }
        else if (!empty($genre_checked)) {
            $data = Event::whereIn('genre_id', $genre_checked)->with('venue')->with('artist')->with('genre')->orderBy('id', 'DESC')->get();
            return view('home')->with('data', $data)->with('genre_checked', $genre_checked)->with('artist', $artist)->with('venue', $venue)->with('genre', $genre);

        }
        else if(!empty($date_split)) {
            $data = Event::whereBetween('date', [$newFromDate, $newToDate])->with('venue')->with('artist')->with('genre')->orderBy('id', 'DESC')->get();
            return view('home')->with('data', $data)->with('artist', $artist)->with('venue', $venue)->with('genre', $genre);

        }


    }
}
