 <?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\VenueController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index']);
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/search',[HomeController::class, 'search'])->name('search');

Route::post('/home/filter/',[HomeController::class, 'filter'])->name('filter');



Route::group(['middleware' => 'auth'], function () {

    Route::get('/artist',[ArtistController::class, 'index'])->name('artist');
    Route::post('/artist/update/{id}',[ArtistController::class, 'update'])->name('artist.update');
    Route::post('/artistAdd',[ArtistController::class, 'store'])->name('artist.add');
    Route::get('/artist/destroy/{id}',[ArtistController::class, 'destroy'])->name('artist.destroy');

    Route::get('/event',[EventController::class, 'index'])->name('event');
    Route::get('/event/add',[EventController::class, 'add'])->name('event.add');
    Route::post('/event/add',[EventController::class, 'store']);
    Route::get('/event/edit/{id}',[EventController::class, 'edit'])->name('event.edit');
    Route::post('/event/edit/{id}',[EventController::class, 'update']);
    Route::get('/event/destroy/{id}',[EventController::class, 'destroy'])->name('event.destroy');

    Route::get('/genre',[GenreController::class, 'index'])->name('genre');
    Route::post('/genreAdd',[GenreController::class, 'store'])->name('genre.add');
    Route::post('/genre/update/{id}',[GenreController::class, 'update'])->name('genre.update');
    Route::get('/genre/destroy/{id}',[GenreController::class, 'destroy'])->name('genre.destroy');

    Route::get('/venue',[VenueController::class, 'index'])->name('venue');
    Route::post('/venue/update/{id}',[VenueController::class, 'update'])->name('venue.update');
    Route::post('/venueAdd',[VenueController::class, 'store'])->name('venue.add');
    Route::get('/venue/destroy/{id}',[VenueController::class, 'destroy'])->name('venue.destroy');

});







// Route::get('/dashboard', function () {return view('dashboard');})->middleware(['auth'])->name('dashboard');

// Route::get('/dashboard', [HomeController::class, 'test'])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

// Auth::routes();

