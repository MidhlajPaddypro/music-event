<?php

namespace Database\Factories;

use App\Models\Venue;
use Illuminate\Database\Eloquent\Factories\Factory;

class VenueFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Venue::class;
    public function definition()
    {
        return [

            'name' => $this->faker->name,
            'contact_number' => $this->faker->randomDigit,
            'address' => $this->faker->text,
        ];
    }
}
