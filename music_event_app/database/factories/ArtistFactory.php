<?php

namespace Database\Factories;

use App\Models\Artist;
use Illuminate\Database\Eloquent\Factories\Factory;


class ArtistFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Artist::class;
    public function definition()
    {
        return [

            'name' => $this->faker->name,
        ];
    }
}
