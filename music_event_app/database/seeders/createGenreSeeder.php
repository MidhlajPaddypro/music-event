<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class createGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('genre_master')->delete();

        DB::table('genre_master')->insert(array (

            1 =>
            array (
                'id' => 1,
                'name' => 'Rock',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            2 =>
            array (
                'id' => 2,
                'name' => 'Pop',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            3 =>
            array (
                'id' => 3,
                'name' => 'Jazz',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            4 =>
            array (
                'id' => 4,
                'name' => 'Blues',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            5 =>
            array (
                'id' => 5,
                'name' => 'Traditional',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            6 =>
            array (
                'id' => 6,
                'name' => 'EDM',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),



        ));
    }
}
