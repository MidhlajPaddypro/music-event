<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(createGenreSeeder::class);
        $this->call(createVenueSeeder::class);
        $this->call(createArtistSeeder::class);
        $this->call(createEventSeeder::class);
        $this->call(createAdminLogin::class);
    }
}
