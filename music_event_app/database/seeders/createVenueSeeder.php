<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class createVenueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('venue')->delete();

        DB::table('venue')->insert(array (

            1 =>
            array (
                'id' => 1,
                'name' => 'Asiatic library step',
                'address' => 'Los Angels,USA 676546',
                'contact_number' => 9097898990,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            2 =>
            array (
                'id' => 2,
                'name' => 'JIO Garden',
                'address' => 'Statue Park,New york 776546',
                'contact_number' => 9097887766,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            3 =>
            array (
                'id' => 3,
                'name' => 'Grease Monkey',
                'address' => 'The Third Square,California 888846',
                'contact_number' => 9096666600,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),




        ));

    }
}
