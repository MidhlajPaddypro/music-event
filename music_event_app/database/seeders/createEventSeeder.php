<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class createEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->delete();

        DB::table('events')->insert(array (

            1 =>
            array (
                'id' => 1,
                'title' => 'Raghu Dixit Live',
                'genre_id' => 1,
                'short_description' => 'In traditional genres such as folk and classical music, a music festival can be defined as a community event with performances of singing and instrument playing that is often presented with a theme such as musical genre  or holiday.',
                'amount' => '1001',
                'date' => '2022-09-09',
                'venue_id' => 3,
                'image'=>'event1.jpg',
                'artist_id' => 1,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            2 =>
            array (
                'id' => 2,
                'title' => 'Ragam Live',
                'genre_id' => 2,
                'short_description' => 'In traditional genres such as folk and classical music, a music festival can be defined blues, folk, jazz, classical music), nationality, locality of musicians, or holiday.',
                'amount' => '700',
                'date' => '2022-06-08',
                'image' => 'E027195.jpg',
                'venue_id' => 2,
                'artist_id' => 2,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
             3 =>
            array (
                'id' => 3,
                'title' => 'Vibranium Live',
                'genre_id' => 3,
                'short_description' => 'In traditional genres such as folk and classical music, a music festival can be defined blues, folk, jazz, classical music), nationality, locality of musicians, or holiday.',
                'amount' => '2700',
                'date' => '2022-06-07',
                'image' => 'E027195.jpg',
                'venue_id' => 3,
                'artist_id' => 2,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
             4 =>
            array (
                'id' => 4,
                'title' => 'Gala Live',
                'genre_id' => 1,
                'short_description' => 'In traditional genres such as folk and classical music, a music festival can be defined blues, folk, jazz, classical music), nationality, locality of musicians, or holiday.',
                'amount' => '7400',
                'date' => '2022-06-08',
                'image' => 'E027195.jpg',
                'venue_id' => 1,
                'artist_id' => 4,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
             5 =>
            array (
                'id' => 52,
                'title' => 'Celestia Live',
                'genre_id' => 4,
                'short_description' => 'In traditional genres such as folk and classical music, a music festival can be defined blues, folk, jazz, classical music), nationality, locality of musicians, or holiday.',
                'amount' => '600',
                'date' => '2022-06-08',
                'image' => 'E027195.jpg',
                'venue_id' => 4,
                'artist_id' => 4,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
             6 =>
            array (
                'id' => 6,
                'title' => 'Ragam Live',
                'genre_id' => 3,
                'short_description' => 'In traditional genres such as folk and classical music, a music festival can be defined blues, folk, jazz, classical music), nationality, locality of musicians, or holiday.',
                'amount' => '500',
                'date' => '2022-06-08',
                'image' => 'E027195.jpg',
                'venue_id' => 1,
                'artist_id' =>1,
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
        ));

    }
}
