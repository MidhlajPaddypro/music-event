<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class createAdminLogin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->delete();

        DB::table('users')->insert(array (
            0 =>
            array (
                'id' =>1,
                'name' => 'AdminName',
                'email' => 'admin@admin.com',
                'password' => Hash::make("123123123"),
                'created_at' => '2018-08-28',
                'updated_at' => '2018-08-28',
            ),
            1 =>
            array (
                'id' =>2,
                'name' => 'AdminSubName',
                'email' => 'adminsub@admin.com',
                'password' => Hash::make("123123123"),
                'created_at' => '2018-08-28',
                'updated_at' => '2018-08-28',
            ),
            2 =>
            array (
                'id' =>3,
                'name' => 'VendorName',
                'email' => 'vendor@admin.com',
                'password' => Hash::make("123123123"),
                'created_at' => '2018-08-28',
                'updated_at' => '2018-08-28',
            ),
        ));


    }
}
