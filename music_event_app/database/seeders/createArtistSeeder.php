<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class createArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_master')->delete();

        DB::table('artist_master')->insert(array (

            1 =>
            array (
                'id' => 1,
                'name' => 'Raghu Dixit',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            2 =>
            array (
                'id' => 2,
                'name' => 'Nucleya',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            3 =>
            array (
                'id' => 3,
                'name' => 'Ritviz',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            4 =>
            array (
                'id' => 4,
                'name' => 'Nirali Kartik',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),
            5 =>
            array (
                'id' => 5,
                'name' => 'Arjith Sing',
                'status'=> 1,
                'created_at' => '2022-04-13',
                'updated_at' => '2022-04-13',
            ),



        ));
    }
}
