<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('genre_id');
            $table->string('image')->nullable();
            $table->longText('short_description');
            $table->double('amount');
            $table->dateTime('date');
            $table->unsignedInteger('venue_id');
            $table->unsignedInteger('artist_id');
            $table->tinyInteger('status')->comment('1-Active / 2-Not Active')->default(1);
            $table->timestamps();
            $table->foreign('venue_id')->references('id')->on('venue')->onDelete('cascade');;
            $table->foreign('genre_id')->references('id')->on('genre_master')->onDelete('cascade');;
            $table->foreign('artist_id')->references('id')->on('artist_master')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
