@extends('master') @section('content')




<div class="col-md-12" style="margin-top:2vw;">
    <form action="{{ route('search') }}" method="GET" role="search">
        <div class="col-md-10 col-sm-10   form-group top_search">
            <div class="input-group">
                <input type="text" id="searchmusic" name="searchmusic" class="form-control" autocomplete="false" placeholder="Search for  Musical Events ...">
                <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit">Go!</button>
                </span>
            </div>
            <script>
                @isset($search)
                document.getElementById("searchmusic").value = "{{ $search }}";
                @endisset

            </script>
        </div>

        <div class="dropdown docs-options col-md-2 col-sm-2 form-group">
            <button type="button" class="btn btn-dark btn-block dropdown-toggle" id="toggleOptions" data-toggle="dropdown" aria-expanded="true">Filter<span class="caret"></span></button>

            <ul class="dropdown-menu" aria-labelledby="toggleOptions" role="menu">
                <li><button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-artist-modal-sm" style="width:100%;text-align:center;">Artist</button></li>
                <li><button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-genre-modal-sm" style="width:100%;text-align:center;"> Genre</button></li>
                <li><button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-venue-modal-sm" style="width:100%;text-align:center;"> Venue</button></li>
                <li><button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-date-modal-sm" style="width:100%;text-align:center;"> Date</button></li>
                <li style="position: -webkit-sticky;position: sticky;bottom: 0;background-color: rgb(255, 255, 255);color: #000000;">
                    <button type="button" onclick="window.location.href='{{ route('home') }}'" class="btn btn-warning" style="width:100%;text-align:center;" type="button"> Clear Filter</button>
                </li>

            </ul>
        </div>
    </form>
</div>


@if($data->count() == 0)

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <h3 style="text-align: center; margin-top:5vw">No Record Found....</h3>
    </div>
</div>
@endif



@foreach ($data as $data_)



<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h4> {{ $data_->title }} <small style="color: rgb(0, 136, 131)">at {{ $data_->venue->name }}</small></h4>
                </div>
            </div>

            <div class="col-md-8 col-sm-8 ">
                <div>
                    <img src="{{ url('uploads/'.$data_->image) }}" width="690" height="300">
                </div>
            </div>
            <div class="col-md-4 col-sm-4  bg-white">
                <div class="x_title">
                    <h2 style="float: none; text-align: center ">Event Details</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-12">
                        <p>Event Name: <span class="green">{{ $data_->title }}</span> </p>
                    </div>
                    <div class="col-md-12">
                        <p>Artist : <span class="green">{{ $data_->artist->name }}</span> </p>
                    </div>
                    <div class="col-md-12">
                        <p>Event Amount : <span class="green">₹ {{ $data_->amount }}</span> </p>
                    </div>
                    <div class="col-md-6">
                        <p>Genre : <span class="green">{{ $data_->genre->name }}</span> </p>
                    </div>
                    <div class="col-md-6">
                        <p>Date : <span class="green">{{date('d-m-Y', strtotime($data_->date))}}</span> </p>
                    </div>

                    <div class="col-md-12">
                        <p>Venue Address : <span class="green">{{ $data_->venue->name }},{{ $data_->venue->address }}</span> </p>
                    </div>
                    <div class="col-md-12">
                        <p>Venue Contact : <span class="green">{{ $data_->venue->contact_number }}</span> </p>
                    </div>

                    <div class="col-md-12">
                        <p>Description : <span class="green">{{ $data_->short_description }}</span> </p>
                    </div>




                </div>


            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<br />
@endforeach

<br />
<div class="row">
    <div class="col-md-12 col-sm-12 " style="text-align: center">
        @if(method_exists($data, 'links'))
        {!! $data->links() !!}
        @endif
    </div>
</div>
<br />

<div class="modal fade bs-artist-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="max-height: 70vh;overflow-y: scroll">
            <div class="modal-header">
                <h4 class="modal-title" id="artistModal">Filter by Artist</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="post" action="{{ route('filter') }}">
                @csrf
                <div class="modal-body">
                    @if(isset($artist))
                    @foreach ($artist as $res )
                    @if(isset($artist_checked) && in_array($res->id, $artist_checked))
                    <div class="checkbox">
                        <label class="">
                            <div class="icheckbox_flat-green checked" style="position: relative;"><input type="checkbox" class="flat" name="artist_checkbox_{{ $res->id }}" value="{{ $res->id }}" style="position: absolute; opacity: 0;" checked><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> &numsp;{{ $res->name}}
                        </label>
                    </div>
                    @else
                    <div class="checkbox">
                        <label class="">
                            <div class="icheckbox_flat-green " style="position: relative;"><input type="checkbox" class="flat" name="artist_checkbox_{{ $res->id }}" value="{{ $res->id }}" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> &numsp;{{ $res->name}}
                        </label>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>
                <div class="modal-footer" style=" position: -webkit-sticky;position: sticky;bottom: 0;background-color: rgb(255, 255, 255);">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade bs-genre-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="max-height: 70vh;overflow-y: scroll">
            <div class="modal-header">
                <h4 class="modal-title" id="genreModal">Filter by Genre</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="post" action="{{ route('filter') }}">
                @csrf
                <div class="modal-body">
                    @if(isset($genre))
                    @foreach ($genre as $res )
                    @if(isset($genre_checked) && in_array($res->id, $genre_checked))
                    <div class="checkbox">
                        <label class="">
                            <div class="icheckbox_flat-green checked" style="position: relative;"><input type="checkbox" class="flat" name="genre_checkbox_{{ $res->id }}" value="{{ $res->id }}" style="position: absolute; opacity: 0;" checked><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> &numsp;{{ $res->name}}
                        </label>
                    </div>
                    @else
                    <div class="checkbox">
                        <label class="">
                            <div class="icheckbox_flat-green " style="position: relative;"><input type="checkbox" class="flat" name="genre_checkbox_{{ $res->id }}" value="{{ $res->id }}" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> &numsp;{{ $res->name}}
                        </label>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>

                <div class="modal-footer" style=" position: -webkit-sticky;position: sticky;bottom: 0;background-color: rgb(255, 255, 255);">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade bs-venue-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="max-height: 70vh;overflow-y: scroll">
            <div class="modal-header">
                <h4 class="modal-title" id="venueModal">Filter by Venue</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="post" action="{{ route('filter') }}">
                @csrf
                <div class="modal-body">
                    @if(isset($venue))
                    @foreach ($venue as $res )
                    @if(isset($venue_checked) && in_array($res->id, $venue_checked))
                    <div class="checkbox">
                        <label class="">
                            <div class="icheckbox_flat-green checked" style="position: relative;"><input type="checkbox" class="flat" name="venue_checkbox_{{ $res->id }}" value="{{ $res->id }}" style="position: absolute; opacity: 0;" checked><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> &numsp;{{ $res->name}}
                        </label>
                    </div>
                    @else
                    <div class="checkbox">
                        <label class="">
                            <div class="icheckbox_flat-green " style="position: relative;"><input type="checkbox" class="flat" name="venue_checkbox_{{ $res->id }}" value="{{ $res->id }}" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> &numsp;{{ $res->name}}
                        </label>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>

                <div class="modal-footer" style=" position: -webkit-sticky;position: sticky;bottom: 0;background-color: rgb(255, 255, 255);">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade bs-date-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="max-height: 70vh;overflow-y: scroll">
            <div class="modal-header">
                <h4 class="modal-title" id="venueModal">Filter by Date</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="post" action="{{ route('filter') }}">
                @csrf
                <div class="modal-body">
                    <div>
                        <input type="text" class="form-control " id="reportrange" name="date_filter" placeholder="Select Date">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

