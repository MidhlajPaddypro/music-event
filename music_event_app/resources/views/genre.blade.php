@extends('master') @section('content')
<div class="row">
    <div class="col-md-12 col-sm-6  ">
        <div class="x_panel">
            @if ($errors->any())
            <br>
            <div class="alert alert-error alert-dismissible">
                @foreach ($errors->all() as $error)
                {{ $error }}<br>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            @elseif(session()->has('successAlert'))
            <div class="alert alert-success alert-dismissible " role="alert"> {{ session()->get('successAlert') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <br>
            @endif
            <div class="x_content">
                @foreach ($data as $res)

                <div class="modal fade bs-genre-{{ $res->id }}-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title">Update Genre </h4>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form method="post" action="{{ route('genre.update', ['id' => $res->id]) }}">
                                @csrf
                                <div class="modal-body">



                                    <div class="form-group col-md-12">
                                        <label for="genreName">Genre Name</label>
                                        <input type="text" class="form-control" name="genreName" value="{{ $res->name }}">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="x_title">
                <h2>Genre Records </h2>
                <small class="pull-right"> <a class="btn btn-sm btn-success" style="float :right;color:white; " data-toggle="modal" data-target=".bs-genre-modal-lg">+ Add Genre</a></small>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-hover table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 50%">Name</th>
                            <th style="width: 20%">Status</th>
                            <th style="width: 20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $count = 1;
                        @endphp

                        @foreach ($data as $res)
                        <tr>
                            <th scope="row">{{ $count++ }}</th>

                            <td>
                                {{ $res->name }}
                            </td>
                            <td>
                                {{ $res->status == 1 ? 'Active':'Not Active' }}
                            </td>



                            <td class="pull-right">

                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-genre-{{ $res->id }}-modal-lg">Edit</button>

                                <a onclick="return confirm('Do you really want to delete this Record?')" href="{{ route('genre.destroy', ['id' => $res->id]) }}" class="btn btn-danger btn-sm">Delete</a>
                            </td>


                        </tr>
                        @endforeach


                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<div class="x_content">


    <div class="modal fade bs-genre-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Add New Genre </h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                </div>

                <form method="post" action="{{ route('genre.add') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group col-md-12">
                            <label for="genreName">Genre Name</label>
                            <input type="text" class="form-control" id="genreName" name="genreName">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>


@endsection

