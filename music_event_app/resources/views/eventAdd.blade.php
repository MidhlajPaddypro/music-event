@extends('master') @section('content')
<div class="row">
    <div class="col-md-12 col-sm-6  ">
        <div class="x_panel">

            @if ($errors->any())
            <br>
            <div class="alert alert-error alert-dismissible">
                @foreach ($errors->all() as $error)
                {{ $error }}<br>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            @elseif(session()->has('successAlert'))
            <div class="alert alert-success alert-dismissible " role="alert"> {{ session()->get('successAlert') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <br>
            @endif
            <div class="x_content">


                <div class="modal fade bs-venue-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title">Add New Venue </h4>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form method="post" action="{{ route('venue.add') }}">
                                @csrf
                                <div class="modal-body">



                                    <div class="form-group col-md-12">
                                        <label for="venueName">Venue Name</label>
                                        <input type="text" class="form-control" id="venueName" name="venueName">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="venueContact">Venue Contact Number</label>
                                        <input type="text" class="form-control" maxlength="10" id="venueContact" name="venueContact">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="venueAddress">Venue Address</label>

                                        <textarea style="height: 200px" id="venueAddress" name="venueAddress" required="required" class="form-control" data-parsley-trigger="keyup" data-parsley-minlength="10" data-parsley-maxlength="100" data-parsley-validation-threshold="10">

                    </textarea>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <div class="x_content">


                <div class="modal fade bs-artist-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title">Add New Artist </h4>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form method="post" action="{{ route('artist.add') }}">
                                @csrf
                                <div class="modal-body">



                                    <div class="form-group col-md-12">
                                        <label for="artistName">Artist Name</label>
                                        <input type="text" class="form-control" id="artistName" name="artistName">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>

            <div class="x_content">


                <div class="modal fade bs-genre-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title">Add New Genre </h4>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                            </div>

                            <form method="post" action="{{ route('genre.add') }}">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group col-md-12">
                                        <label for="genreName">Genre Name</label>
                                        <input type="text" class="form-control" id="genreName" name="genreName">
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>


            <div class="x_title">
                <h2> Add New Event Record </h2>

                <small class="pull-right"><button class="btn btn-dark" onclick="window.location.href='{{ route('event') }}'" type="button">Back</button></small>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <form data-parsley-validate class="form-horizontal form-label-left" method="post" id="event_form">
                    {{ csrf_field() }}

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="eventTitle">Event Name <span class="required" </span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" class="form-control" id="eventTitle" name="eventTitle">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="genre">Genre <span class="required" </span>
                        </label>

                        <div class="col-md-6 col-sm-6 ">
                            <select class="form-control" name="genre" id="genre">
                                <option>Choose option </option>
                                @foreach ($genre as $gen)
                                <option value="{{ $gen->id }}">{{ $gen->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-genre-modal-lg">Add New Genre</button>

                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="artist">Artist <span class="required" </span>
                        </label>

                        <div class="col-md-6 col-sm-6 ">
                            <select class="form-control" name="artist" id="artist">
                                <option>Choose option </option>
                                @foreach ($artist as $art)
                                <option value="{{ $art->id }}">{{ $art->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-artist-modal-lg">Add New Artist</button>


                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="venue">Venue <span class="required" </span>
                        </label>

                        <div class="col-md-6 col-sm-6 ">
                            <select class="form-control" name="venue" id="venue">
                                <option>Choose option </option>
                                @foreach ($venue as $ven)
                                <option value="{{ $ven->id }}">{{ $ven->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target=".bs-venue-modal-lg">Add New Venue</button>


                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="date">Date <span class="required" </span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input class="date-picker form-control" name="date" id="date" type="date" required="required" onfocus="this.type='date'" onmouseover="this.type='date'" onclick="this.type='date'" onblur="this.type='text'" onmouseout="timeFunctionLong(this)">
                            <script>
                                function timeFunctionLong(input) {
                                    setTimeout(function() {
                                        input.type = 'text';
                                    }, 60000);
                                }

                            </script>

                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="amount">Amount <span class="required" </span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="description">Short Description <span class="required" </span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">

                            <textarea style="height: 200px" id="description" name="description" required="required" class="form-control" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="300" data-parsley-validation-threshold="10">

                            </textarea>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="image">Image <span class="required" </span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">


                            <input id="inp_img" name="img" type="hidden" value="">
                            <br>

                            <input type="file" name="inp_files" id="inp_files" multiple="multiple">
                            <br>

                            <img id="imageHolder" alt="add image" height="250px" width="100%" />

                        </div>
                    </div>





                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-6 col-sm-6 offset-md-3">
                            <button class="btn btn-primary" onclick="window.location.href='{{ route('event') }}'" type="button">Cancel</button>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>



                </form>



            </div>
        </div>
    </div>
</div>



<script>
    document.getElementById('inp_files').addEventListener('change', fileChange, false);

    function fileChange(e) {
        document.getElementById('inp_img').value = '';
        for (var i = 0; i < e.target.files.length; i++) {
            var file = e.target.files[i];
            if (file.type == "image/jpeg" || file.type == "image/png") {
                var reader = new FileReader();
                reader.onload = function(readerEvent) {
                    var image = new Image();
                    image.onload = function(imageEvent) {
                        var max_size = 600;
                        var w = image.width;
                        var h = image.height;

                        if (w > h) {
                            if (w > max_size) {
                                h *= max_size / w;
                                w = max_size;
                            }
                        } else {
                            if (h > max_size) {
                                w *= max_size / h;
                                h = max_size;
                            }
                        }
                        var canvas = document.createElement('canvas');
                        canvas.width = w;
                        canvas.height = h;
                        canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                        if (file.type == "image/jpeg") {
                            var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                        } else {
                            var dataURL = canvas.toDataURL("image/png");
                        }
                        document.getElementById('inp_img').value += dataURL + '|';
                    }
                    image.src = readerEvent.target.result;
                }
                reader.readAsDataURL(file);
                readURL(this);
            } else {
                document.getElementById('inp_files').value = '';
                alert('Please only select images in JPG or PNG format.');
                return false;
            }
        }
    };


    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imageHolder').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    };

</script>
@endsection

