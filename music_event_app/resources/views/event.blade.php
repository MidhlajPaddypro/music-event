@extends('master') @section('content')
<div class="row">
    <div class="col-md-12 col-sm-6  ">
        <div class="x_panel">

            @if ($errors->any())
            <br>
            <div class="alert alert-error alert-dismissible">
                @foreach ($errors->all() as $error)
                {{ $error }}<br>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            @elseif(session()->has('successAlert'))
            <div class="alert alert-success alert-dismissible " role="alert"> {{ session()->get('successAlert') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <br>
            @endif

            <div class="x_title">
                <h2>Event Records </h2>
                <small class="pull-right"> <a class="btn btn-sm btn-success" style="float :right;color:white; " href="{{ route('event.add') }}">+ Add Event</a></small>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-hover  table-striped" id="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Genre</th>
                            <th>Artist</th>
                            <th>Venue</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $count = 1;
                        @endphp

                        @foreach ($data as $res)
                        <tr>
                            <th scope="row">{{ $count++ }}</th>

                            <td>
                                {{ $res->title }}
                            </td>
                            <td>
                                {{ $res->genre->name }}
                            </td>
                            <td>
                                {{ $res->artist->name }}
                            </td>
                            <td>
                                {{ $res->venue->name }}
                            </td>
                            <td>
                                {{date('d-m-Y', strtotime($res->date))}}
                            </td>
                            <td>
                                ₹ {{ $res->amount }}
                            </td>

                            <td class="pull-right">
                                <a class="btn btn-info btn-sm" href="{{ route('event.edit', ['id' => $res->id]) }}">Edit</a>
                                <a onclick="return confirm('Do you really want to delete this Record?')" href="{{ route('event.destroy', ['id' => $res->id]) }}" class="btn btn-danger btn-sm">Delete</a>
                            </td>


                        </tr>
                        @endforeach


                    </tbody>
                </table>

            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-sm-12 " style="text-align: center">
                    {!! $data->links('pagination::bootstrap-4') !!}
                </div>
            </div>
            <br />
        </div>
    </div>
</div>



@endsection

